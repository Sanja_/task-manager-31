package ru.karamyshev.taskmanager.bootstrap;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.api.ISessionService;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.exception.CommandIncorrectException;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope("singleton")
public class Bootstrap {

/*    @Autowired
    private List<AbstractCommand> commandList;*/

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private AuthenticationEndpointService authenticationEndpointService;

    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @Autowired
    private SessionEndpointService sessionEndpointService;

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ProjectEndpointService projectEndpointService;

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Autowired
    private TaskEndpointService taskEndpointService;

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Autowired
    private UserEndpointService userEndpointService;

    @Autowired
    private UserEndpoint userEndpoint;

    @Autowired
    private AdminEndpointService adminEndpointService;

    @Autowired
    private AdminEndpoint adminEndpoint;

    @Autowired
    private AdminUserEndpointService adminUserEndpointService;

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Nullable
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @Nullable
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @Autowired
    private void init(@NotNull final List<AbstractCommand> commands) {
        for (AbstractCommand command : commands) {
            registry(command);
        }
    }

    public void run(@Nullable final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        inputCommand();
    }

    @SneakyThrows
    private void inputCommand() {
        while (true) {
            try {
                parsCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commands.put(command.name(), command);
        arguments.put(command.arg(), command);
    }

    private void parsCommand(@Nullable final String args) {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) chooseResponsCommand(arg);
    }

    private void parsArgs(final String... args) {
        validateArgs(args);
        try {
            for (String arg : args) chooseResponsArg(arg.trim());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }

    }

    private void validateArgs(@Nullable final String... args) {
        if (args != null || args.length > 0) return;
        System.out.println(MsgCommandConst.COMMAND_ABSENT);
    }

    @SneakyThrows
    private void chooseResponsArg(@Nullable final String arg) {
        @Nullable final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new CommandIncorrectException(arg);
        argument.execute();
    }

    @SneakyThrows
    private void chooseResponsCommand(@Nullable final String cmd) {
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new CommandIncorrectException(cmd);
        command.execute();
    }

}
