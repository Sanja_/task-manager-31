package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.api.ISessionService;
import ru.karamyshev.taskmanager.endpoint.Session;

@Component
public class SessionService implements ISessionService {

    @Nullable
    private Session session;

    @Override
    public void setSession(@Nullable final Session session) {
        this.session = session;
    }

    @Override
    public void clearSession() {
        session = null;
    }

    @Nullable
    @Override
    public Session getSession() {
        return session;
    }

}
