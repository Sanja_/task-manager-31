package ru.karamyshev.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Session;

@Component
public interface ISessionService {

    @Nullable
    void setSession(Session session);

    void clearSession();

    @Nullable
    Session getSession();

}
