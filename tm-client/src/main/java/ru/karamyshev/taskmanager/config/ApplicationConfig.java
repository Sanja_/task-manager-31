package ru.karamyshev.taskmanager.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.karamyshev.taskmanager.endpoint.*;

@Configuration
@ComponentScan("ru.karamyshev.taskmanager")
public class ApplicationConfig {

    @NotNull
    @Bean
    public AdminUserEndpointService adminUserEndpointService() {
        return new AdminUserEndpointService();
    }

    @NotNull
    @Bean
    public AdminUserEndpoint adminUserEndpoint(
            @NotNull final AdminUserEndpointService adminUserEndpointService
    ) {
        return adminUserEndpointService.getAdminUserEndpointPort();
    }

    @NotNull
    @Bean
    public AdminEndpointService adminEndpointService() {
        return new AdminEndpointService();
    }

    @NotNull
    @Bean
    public AdminEndpoint adminEndpoint(
            @NotNull final AdminEndpointService adminEndpointService
    ) {
        return adminEndpointService.getAdminEndpointPort();
    }

    @NotNull
    @Bean
    public AuthenticationEndpointService authenticationEndpointService() {
        return new AuthenticationEndpointService();
    }

    @NotNull
    @Bean
    public AuthenticationEndpoint authenticationEndpoint(
            @NotNull final AuthenticationEndpointService authenticationEndpointService
    ) {
        return authenticationEndpointService.getAuthenticationEndpointPort();
    }

    @NotNull
    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @NotNull
    @Bean
    public ProjectEndpoint projectEndpoint(
            @NotNull final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @NotNull
    @Bean
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @NotNull
    @Bean
    public SessionEndpoint sessionEndpoint(
            @NotNull final SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @NotNull
    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @NotNull
    @Bean
    public TaskEndpoint taskEndpoint(
            @NotNull final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

    @NotNull
    @Bean
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @NotNull
    @Bean
    public UserEndpoint userEndpoint(
            @NotNull final UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }

}
