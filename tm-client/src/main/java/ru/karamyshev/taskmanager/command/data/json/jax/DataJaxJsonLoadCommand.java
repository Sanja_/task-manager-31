package ru.karamyshev.taskmanager.command.data.json.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class DataJaxJsonLoadCommand extends AbstractDataCommand {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-jax-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load to json(jax-b) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON(JAX-B) LOAD]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.loadDataJaxBJson(session);
        System.out.println("[OK]");
        System.out.println("[YOU ARE LOGGED OUT]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
