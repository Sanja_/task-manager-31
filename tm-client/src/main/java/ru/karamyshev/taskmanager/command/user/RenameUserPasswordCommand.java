package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.AuthenticationEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class RenameUserPasswordCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-rnm-psswrd";
    }

    @NotNull
    @Override
    public String name() {
        return "rename-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Rename password account.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = sessionService.getSession();
        System.out.println("CHANGE ACCOUNT PASSWORD");
        System.out.println("[ENTER OLD PASSWORD]");
        @Nullable final String oldPassword = TerminalUtil.nextLine();
        System.out.println("[ENTER NEW PASSWORD]");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        authenticationEndpoint.renamePassword(session, oldPassword, newPassword);
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
