package ru.karamyshev.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.AdminUserEndpoint;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class RegistryUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-rgstr";
    }

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @NotNull
    @Override
    public String description() {
        return "Registration new account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL");
        @Nullable final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @Nullable final String password = TerminalUtil.nextLine();
        adminUserEndpoint.createUserWithEmail(login, password, email);
        System.out.println("[OK]");
    }

}
