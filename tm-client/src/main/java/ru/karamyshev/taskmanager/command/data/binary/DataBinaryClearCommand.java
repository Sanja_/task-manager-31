package ru.karamyshev.taskmanager.command.data.binary;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.service.SessionService;

import java.io.Serializable;

@Component
public class DataBinaryClearCommand extends AbstractDataCommand implements Serializable {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "data-bin-clear";
    }

    @NotNull
    @Override
    public String name() {
        return "-dtbnclr";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove binary file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE BINARY FILE]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.clearDataBinary(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
