package ru.karamyshev.taskmanager.command.info;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.List;

@Component
public class HelpCommand extends AbstractCommand {

   /* @NotNull
    @Autowired
    private CommandService commandService;
*/

    @Nullable
    @Autowired
    List<AbstractCommand> commandsList;

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("\n [HELP]");
        // @Nullable final List<AbstractCommand> commands = commandService.getTerminalCommands();
        for (final AbstractCommand command : commandsList) {
            System.out.println(command.name() + " (" + command.arg() + ") : -" + command.description());
        }
        System.out.println("[OK]");
    }

}
