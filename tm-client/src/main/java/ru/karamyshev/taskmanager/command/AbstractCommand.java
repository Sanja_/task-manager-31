package ru.karamyshev.taskmanager.command;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Role;

@Component
public abstract class AbstractCommand {

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute() throws Exception;

}
