package ru.karamyshev.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionEndpoint;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class ExitCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = sessionService.getSession();
        sessionEndpoint.closeSession(session);
        sessionService.clearSession();
        System.exit(0);
    }

}
