package ru.karamyshev.taskmanager.command.info;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;

@Component
public class VersionShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("\n [VERSION]");
        System.out.println("1.0.8");
    }

}
