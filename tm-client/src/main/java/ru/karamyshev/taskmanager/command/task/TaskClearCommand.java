package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.TaskEndpoint;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-tskclr";
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASKS]");
        @Nullable final Session session = sessionService.getSession();
        taskEndpoint.clearTask(session);
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
