package ru.karamyshev.taskmanager.command.data.xml.faster;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.service.SessionService;

@Component
public class DataFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-fs-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data from xml(faster) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML(FASTER) SAVE]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.saveDataFasterXml(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
