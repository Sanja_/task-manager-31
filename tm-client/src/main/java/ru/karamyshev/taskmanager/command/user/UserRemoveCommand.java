package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.UserEndpoint;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class UserRemoveCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-rmvusr";
    }

    @NotNull
    @Override
    public String name() {
        return "remove-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove users";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[REMOVE USER]");
        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        userEndpoint.removeUserByLogin(session, login);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
