package ru.karamyshev.taskmanager.command.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.service.SessionService;

import java.io.Serializable;

@Component
public class DataBase64ClearCommand extends AbstractDataCommand implements Serializable {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Override
    public String arg() {
        return "-dtbsclr";
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove base64 file.";
    }


    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE BASE64 FILE]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.clearDataBase64(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
