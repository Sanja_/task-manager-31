package ru.karamyshev.taskmanager.command.info;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.List;

@Component
public class CommandsShowCommand extends AbstractCommand {

  /*  @NotNull
    @Autowired
    private CommandService commandService;
*/
  @Nullable
  @Autowired
  List<AbstractCommand> commandsList;

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        System.out.println("\n [COMMANDS]");
       // @Nullable final List<AbstractCommand> commandsList = commandService.getTerminalCommands();
        for (final AbstractCommand command : commandsList) System.out.println(command.name());
    }

}
