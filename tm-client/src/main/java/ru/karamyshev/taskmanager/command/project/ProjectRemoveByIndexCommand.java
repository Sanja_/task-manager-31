package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Project;
import ru.karamyshev.taskmanager.endpoint.ProjectEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

@Component
public class ProjectRemoveByIndexCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-prtrmvind";
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT INDEX FOR DELETION:");
        @Nullable final Integer index = TerminalUtil.nextNumber();
        @Nullable final Project project = projectEndpoint.removeOneProjectByIndex(session, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
