package ru.karamyshev.taskmanager.command.data.json.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.AdminEndpoint;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.service.SessionService;

import java.io.Serializable;

@Component
public class DataJaxJsonClearCommand extends AbstractDataCommand implements Serializable {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-jax-json-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove json(jax-b) file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE JSON(JAX-V) FILE]");
        @Nullable final Session session = sessionService.getSession();
        adminEndpoint.clearDataJaxBJson(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
