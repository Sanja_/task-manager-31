package ru.karamyshev.taskmanager.command.info;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.List;

@Component
public class ArgumentsShowCommand extends AbstractCommand {

/*
    @NotNull
    @Autowired
    private CommandService commandService;
*/

    @Nullable
    @Autowired
    List<AbstractCommand> commandsList;


    @NotNull
    @Override
    public String arg() {
        return "-agr";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        System.out.println("\n [ARGUMENTS]");
        //  @Nullable final List<AbstractCommand> commandsList = commandService.getTerminalCommands();
        for (final AbstractCommand command : commandsList) System.out.println(command.arg());
    }

}
