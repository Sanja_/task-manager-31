package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.IAuthenticationEndpoint;
import ru.karamyshev.taskmanager.api.service.IAuthenticationService;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class AuthenticationEndpoint extends AbstractEndpoint implements IAuthenticationEndpoint {

    @Nullable
    @Autowired
    private IAuthenticationService authenticationService;

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IUserService userService;

    @Override
    @WebMethod
    public void login(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) {
        authenticationService.login(login, password);
    }

    @Nullable
    @Override
    @WebMethod
    public String getUserId(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        return authenticationService.getUserId();
    }

    @Nullable
    @Override
    @WebMethod
    public void checkRoles(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "role", partName = "role") @Nullable Role[] roles
    ) throws Exception {
        sessionService.validate(session);
        authenticationService.checkRoles(roles);
    }

    @Nullable
    @Override
    @WebMethod
    public String getCurrentLogin(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception {
        sessionService.validate(session);
        return authenticationService.getCurrentLogin();
    }

    @Override
    @WebMethod
    public boolean isAuth() {
        return authenticationService.isAuth();
    }

    @Override
    @WebMethod
    public void registryUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws Exception {
        authenticationService.registry(login, password, email);
    }

    @Override
    @WebMethod
    public void renameLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "newLogin", partName = "newLogin") @Nullable String newLogin
    ) throws Exception {
        sessionService.validate(session);
        final User currentUser = userService.findById(session.getUserId());
        authenticationService.renameLogin(session.getUserId(), currentUser.getLogin(), newLogin);
    }

    @Nullable
    @Override
    @WebMethod
    public User showProfile(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        sessionService.validate(session);
        return authenticationService.showProfile(session.getUserId(), login);
    }

    @Override
    @WebMethod
    public void renamePassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "oldPassword", partName = "oldPassword") @Nullable final String oldPassword,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) throws Exception {
        sessionService.validate(session);
        final User currentUser = userService.findById(session.getUserId());
        authenticationService
                .renamePassword(session.getUserId(), currentUser.getLogin(), oldPassword, newPassword);
    }

    @Override
    @WebMethod
    public void logout() {
        authenticationService.logout();
    }

}
