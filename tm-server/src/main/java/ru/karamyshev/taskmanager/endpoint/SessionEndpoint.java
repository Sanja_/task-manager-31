package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.ISessionEndpoint;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.dto.Fail;
import ru.karamyshev.taskmanager.dto.Result;
import ru.karamyshev.taskmanager.dto.Success;
import ru.karamyshev.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService
@Controller
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "loginSession") @Nullable final String login,
            @WebParam(name = "password", partName = "passwordSession") @Nullable final String password
    ) throws Exception {
        return sessionService.open(login, password);
    }


    @Nullable
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        sessionService.validate(session);
        try {
            sessionService.close(session);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public Result closeAllSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        sessionService.validate(session);
        try {
            sessionService.closeAll(session);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

}
