package ru.karamyshev.taskmanager.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.karamyshev.taskmanager")
public class ApplicationConfig {
}
