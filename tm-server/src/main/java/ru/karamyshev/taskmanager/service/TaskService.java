package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.exception.IndexIncorrectException;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.empty.IdEmptyException;
import ru.karamyshev.taskmanager.exception.empty.NameEmptyException;
import ru.karamyshev.taskmanager.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) return;
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) return;
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        taskRepository.remove(userId, task);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        return taskRepository.findAll(userId);
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        taskRepository.clear(userId);
    }

    @Nullable
    @Override
    public Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        return taskRepository.findOneByIndex(userId, index - 1);
    }

    @Nullable
    @Override
    public List<Task> findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        return taskRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public Task updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setId(Long.parseLong(id));
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index <= 0) throw new IndexIncorrectException();
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        return taskRepository.removeOneByIndex(userId, index - 1);
    }

    @Nullable
    @Override
    public List<Task> removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        return taskRepository.removeOneByName(userId, name);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        return taskRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public Task removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ITaskRepository taskRepository = context.getBean(TaskRepository.class);
        return taskRepository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public Task updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
