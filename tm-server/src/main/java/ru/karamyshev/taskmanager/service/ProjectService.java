package ru.karamyshev.taskmanager.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.exception.IndexIncorrectException;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.empty.IdEmptyException;
import ru.karamyshev.taskmanager.exception.empty.NameEmptyException;
import ru.karamyshev.taskmanager.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @Override
    public void create(@Nullable String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Project project = new Project();
        project.setName(name);
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(@Nullable String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        projectRepository.remove(userId, project);
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        return projectRepository.findAll(userId);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        projectRepository.clear(userId);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index <= 0) throw new IndexIncorrectException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        return projectRepository.findOneByIndex(userId, index - 1);
    }

    @Nullable
    @Override
    public List<Project> findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        return projectRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public Project updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(Long.parseLong(id));
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index <= 0) throw new IndexIncorrectException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        return projectRepository.removeOneByIndex(userId, index - 1);
    }

    @Nullable
    @Override
    public List<Project> removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        return projectRepository.removeOneByName(userId, name);
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        return projectRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public Project removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        return projectRepository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public Project updateProjectByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
