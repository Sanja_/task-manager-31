package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.karamyshev.taskmanager.api.repository.ISessionRepository;
import ru.karamyshev.taskmanager.api.service.IPropertyService;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.exception.user.AccessDeniedException;
import ru.karamyshev.taskmanager.repository.SessionRepository;
import ru.karamyshev.taskmanager.util.HashUtil;
import ru.karamyshev.taskmanager.util.SignatureUtil;

import java.util.List;

@Service
public class SessionService extends AbstractService<Session> implements ISessionService {

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private IPropertyService propertyService;

    @Override
    public void close(@Nullable final Session session) throws Exception {
        validate(session);
        @Nullable final ISessionRepository sessionRepository = context.getBean(SessionRepository.class);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Override
    public void closeAll(@Nullable final Session session) throws Exception {
        validate(session);
        @Nullable final ISessionRepository sessionRepository = context.getBean(SessionRepository.class);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public User getUser(@Nullable final Session session) throws Exception {
        final String userId = getUserId(session);
        return userService.findById(userId);
    }

    @Nullable
    @Override
    public String getUserId(@Nullable final Session session) throws Exception {
        validate(session);
        return session.getUserId();
    }

    @Nullable
    @Override
    public List<Session> getListSession(@Nullable final Session session) throws Exception {
        validate(session);
        @Nullable final ISessionRepository sessionRepository = context.getBean(SessionRepository.class);
        return sessionRepository.findAllSession(session.getUserId());
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        @Nullable final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @Nullable final ISessionRepository sessionRepository = context.getBean(SessionRepository.class);
        if (!sessionRepository.contains(session.getUserId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) throws Exception {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        @Nullable final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return null;
        @Nullable final Session session = new Session();
        session.setUserId(Long.toString(user.getId()));
        session.setTimestamp(System.currentTimeMillis());
        @Nullable final ISessionRepository sessionRepository = context.getBean(SessionRepository.class);
        sessionRepository.add(session);
        return sign(session);
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        final User user = userService.findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void signOutByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String userId = Long.toString(user.getId());
        @Nullable final ISessionRepository sessionRepository = context.getBean(SessionRepository.class);
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final ISessionRepository sessionRepository = context.getBean(SessionRepository.class);
        sessionRepository.removeByUserId(userId);
    }

}
