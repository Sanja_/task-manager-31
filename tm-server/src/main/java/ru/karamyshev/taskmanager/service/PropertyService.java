package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.karamyshev.taskmanager.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    private final String file = "/application.properties";

    @Nullable
    private final Properties properties = new Properties();

    @Override
    public void init() {
        @Nullable final InputStream inputStream = PropertyService.class.getResourceAsStream(file);
        try {
            properties.load(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String propertyHost = properties.getProperty("server.host");
        @NotNull final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String propertyPort = properties.getProperty("server.port");
        @NotNull final String envPort = System.getProperty("server.port");
        @NotNull String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

}
