package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.karamyshev.taskmanager.api.service.IDomainService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.Domain;

@Service
public class DomainService implements IDomainService {

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private ITaskService taskService;

    @Nullable
    @Autowired
    private IProjectService projectService;

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getList());
        domain.setTasks(taskService.getList());
        domain.setUsers(userService.getList());
    }

}
